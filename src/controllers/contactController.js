const {
  createContactRequestValidator,
  updateContactRequestValidator,
  getContactRequestValidator,
  getContactsRequestValidator,
  deleteContactRequestValidator,
  validateWithJoiSchema
} = require('../utils/validators')

const {
  createNewContact, updateContractById,
  findContractById, deleteContactById, listContacts
} = require('../repositories/contactRepository')

async function createContact(data) {
  try {
    data.date = new Date();
    validateWithJoiSchema(data, createContactRequestValidator)
    const result = await createNewContact(data)
    return result
  } catch (e) {
    console.log("createContact error ", e)
    throw e
  }
}

async function updateContact(contactId, data) {
  try {
    validateWithJoiSchema({...data, contactId}, updateContactRequestValidator)
    const result = await updateContractById(contactId, data)
    return result
  } catch (e) {
    console.log("updateContact error ", e)
    throw e
  }
}

async function deleteContact(contactId) {
  try {
    validateWithJoiSchema({ contactId}, deleteContactRequestValidator)
    const result = await deleteContactById(contactId)
    return result
  } catch (e) {
    console.log("updateContact error ", e)
    throw e
  }
}

async function getContactDetail(contactId) {
  try {
    validateWithJoiSchema({contactId}, getContactRequestValidator)
    const result = await findContractById(contactId)
    return result
  } catch (e) {
    console.log("updateContact error ", e)
    throw e
  }
}

async function getContracts(begin , total ) {
  try {
    validateWithJoiSchema({begin, total}, getContactsRequestValidator)
    const result = await listContacts(begin, total)
    return result
  } catch (e) {
    console.log("updateContact error ", e)
    throw e
  }
}

module.exports = {
  updateContact, createContact,
  getContactDetail, deleteContact,
  getContracts
}
