const mongoose = require('mongoose');
const  mongoUrl ="mongodb://"+ process.env.MONGO_USERNAME + ":" + process.env.MONGO_PASSWORD + "@" + process.env.MONGO_HOST + ":" + process.env.MONGO_PORT + "/" + process.env.MONGO_DB_NAME;

const initMongo = () => {
  console.log("initMongo() called ", {mongoUrl})
  mongoose.connect(mongoUrl, {useNewUrlParser: true});
  const db = mongoose.connection;
  db.on('error',
    (e) => {
      console.log('db connection error...', e)
      throw e
    });
  db.once('open', () => {
    console.log('db opened ...',mongoUrl);
  });
};

module.exports = {initMongo}