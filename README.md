### Run locally
1. `npm i`
2. `npm start`
3. you can check http://localhost:3005/api-docs

### Deploy Manually
1. `heroku container:login`
2. Push docker image to heroku `heroku container:push --app karimi-test  --context-path . web`
3. Release image `heroku container:release --app karimi-test web`
4. then you can check https://karimi-test.herokuapp.com/api-docs

### CI/CD
Just push on gitlab, then docker image will be create and deploy on Heroku