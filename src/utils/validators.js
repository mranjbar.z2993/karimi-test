const Joi = require('@hapi/joi')
const ERROR_MESSAGES = require('../utils/messages')
//I don't know the real pattern of phoneNumbers , country code is reqiured or not , ...
const mobileRegex = new RegExp("[0-9+]{11,13}$")
const mongoIdRegex = new RegExp('^(?=[a-f\\d]{24}$)(\\d+[a-f]|[a-f]+\\d)')
const {createStandardError} = require('../utils/errorHandler')

const contactIdValidator = Joi.string()
  .required()
  .pattern(mongoIdRegex)
  .messages({
    'string.base': ERROR_MESSAGES.INVALID_CONTACT_ID,
    'string.pattern.base': ERROR_MESSAGES.INVALID_CONTACT_ID,
    'any.required': ERROR_MESSAGES.REQUIRED_CONTACT_ID
  })

const throwHttpErrorIfJoiValidatorFails = (validationResult) => {
  if (validationResult.error) {
    // console.log("convertJoiErrorToHttpError", {details: validationResult.error.details})
    throw  createStandardError(validationResult.error.details[0].message,
      400);
  }
};

const validateWithJoiSchema = (data, schema) => {
  const validationResult = schema.validate(data)
  throwHttpErrorIfJoiValidatorFails(validationResult)
};

const createContactRequestValidator = Joi.object({
  phone: Joi.string()
    .required()
    .pattern(mobileRegex)
    .messages({
      'string.base': ERROR_MESSAGES.INVALID_PHONE,
      'string.pattern.base': ERROR_MESSAGES.INVALID_PHONE,
      'any.required': ERROR_MESSAGES.REQUIRED_PHONE
    }),

  lastName: Joi.string()
    .required()
    .messages({
      'string.base': ERROR_MESSAGES.INVALID_LAST_NAME,
      'any.required': ERROR_MESSAGES.REQUIRED_LAST_NAME
    }),

  firstName: Joi.string()
    .required()
    .messages({
      'string.base': ERROR_MESSAGES.INVALID_FIRST_NAME,
      'any.required': ERROR_MESSAGES.REQUIRED_FIRST_NAME
    }),
  email: Joi.string()
    .email()
    .required()
    .messages({
      'string.base': ERROR_MESSAGES.INVALID_EMAIL,
      'any.required': ERROR_MESSAGES.REQUIRED_EMAIL
    }),
  country: Joi.string()
    .valid("Australia", "Canada", "United Kingdom")
    .required()
    .messages({
      'string.base': ERROR_MESSAGES.INVALID_COUNTRY,
      'any.required': ERROR_MESSAGES.REQUIRED_COUNTRY,
      'any.only': ERROR_MESSAGES.COUNTRY_SHOULD_BE_CANADA_AUSTRALIA_OR_UK,

    }),

  age: Joi.number()
    .min(1)
    .required()
    .messages({
      'number.base': ERROR_MESSAGES.INVALID_AGE,
      'any.required': ERROR_MESSAGES.REQUIRED_AGE
    }),
  date: Joi.date()
    .min(1)
    .required()
    .messages({
      'date.base': ERROR_MESSAGES.INVALID_DATE,
      'any.required': ERROR_MESSAGES.REQUIRED_DATE
    }),
})

const updateContactRequestValidator = Joi.object({
  contactId: contactIdValidator,

  phone: Joi.string()
    .pattern(mobileRegex)
    .messages({
      'string.base': ERROR_MESSAGES.INVALID_PHONE,
      'string.pattern.base': ERROR_MESSAGES.INVALID_PHONE,
      'any.required': ERROR_MESSAGES.REQUIRED_PHONE
    }),

  lastName: Joi.string()
    .messages({
      'string.base': ERROR_MESSAGES.INVALID_LAST_NAME,
    }),

  firstName: Joi.string()
    .messages({
      'string.base': ERROR_MESSAGES.INVALID_FIRST_NAME,
    }),
  email: Joi.string()
    .email()
    .messages({
      'string.base': ERROR_MESSAGES.INVALID_EMAIL,
    }),
  country: Joi.string()
    .valid("Australia", "Canada", "United Kingdom")
    .messages({
      'string.base': ERROR_MESSAGES.INVALID_COUNTRY,
      'any.only': ERROR_MESSAGES.COUNTRY_SHOULD_BE_CANADA_AUSTRALIA_OR_UK,

    }),

  age: Joi.number()
    .min(1)
    .messages({
      'number.base': ERROR_MESSAGES.INVALID_AGE,
    }),
  date: Joi.date()
    .messages({
      'date.base': ERROR_MESSAGES.INVALID_DATE,
    }),
})

const deleteContactRequestValidator = Joi.object({
  contactId: contactIdValidator,
})
const getContactRequestValidator = Joi.object({
  contactId: contactIdValidator,

})

const getContactsRequestValidator = Joi.object({

  begin: Joi.number()
    .min(0)
    .messages({
      'number.base': ERROR_MESSAGES.INVALID_BEGIN,
    }),

  total: Joi.number()
    .min(1)
    .max(100)
    .messages({
      'number.base': ERROR_MESSAGES.INVALID_TOTAL,
    })
})

module.exports = {
  createContactRequestValidator,
  updateContactRequestValidator,
  getContactRequestValidator,
  getContactsRequestValidator,
  deleteContactRequestValidator,
  validateWithJoiSchema
}