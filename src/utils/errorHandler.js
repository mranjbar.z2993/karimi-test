/**
 * @param err
 * @param req
 * @param res
 * @param next : not used but if you delete that the express
 doesnt know this middleware is error handler
 * @returns {Promise<void>}
 * Global errorHandler if in routes throw exception this middlware should call
 */
const errorHandler = async (err, req, res, next) => {
  const data = {
    message: err.message
  };
  res.status(err.status || 500).json(data)
}

const createStandardError = (message, status = 500) => {
  const error = new Error(message);
  message.status = status;
  return error
}

module.exports = {
  errorHandler,
  createStandardError
}