const mongoConnector = require('./db/mongoConnector')
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const router = require('./routers/contactRouter')
const {errorHandler} = require('./utils/errorHandler')


const initSwagger = () => {
  const swaggerUi = require('swagger-ui-express');
  const swaggerDocument = require('./swagger.json');
// Because production environment support ssl we should scheme of swaggerData to https to can load that in production
  if (process.env.NODE_ENV === 'production') {
    swaggerDocument.schemes = 'https';
  }
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
};

function initServer() {
  app.use(cors())
  app.use(bodyParser())
  mongoConnector.initMongo()

  const health = (req, res) => {
    res.json({
      message: 'Hoooraaa I am alive'
    })
  }

  app.get('/health', health);
  app.get('/', health);


  initSwagger()
  app.use(router)
  app.use(errorHandler)
  app.listen(process.env.PORT, () => {
    console.log("listening to ", process.env.PORT)
  })
}

module.exports = {
  initServer
}