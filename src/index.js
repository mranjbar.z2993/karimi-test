if (process.env.NODE_ENV !== 'production') {
  //in production mode we set environment variables in heroku dashboard
  require('dotenv').config();
}
const {initServer} = require('./server')
initServer()