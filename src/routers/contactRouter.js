const express = require('express');
const router = express.Router();
const {
  updateContact, createContact,
  getContactDetail, deleteContact,
  getContracts
} = require('../controllers/contactController')
const ERROR_MESSAGES = require('../utils/messages')

router.post('/contacts', async (req, res, next) => {
    try {
      const result = await createContact(req.body)
      res.send(result)
    } catch (e) {
      console.log("POST /contacts error", e)
      next(e)
    }
  }
);

router.put('/contacts/:contactId', async (req, res, next) => {
    try {
      const result = await updateContact(req.params.contactId, req.body)
      if (!result) {
        res.status(404)
        return res.send({
          message: ERROR_MESSAGES.CONTACT_NOT_FOUND
        })
      }

      res.send(result)
    } catch (e) {
      console.log("PUT /contacts error", e)
      next(e)
    }
  }
);

router.delete('/contacts/:contactId', async (req, res, next) => {
    try {
      const result = await deleteContact(req.params.contactId)
      console.log("delete contact ", {result})
      if (result.n === 0) {
        res.status(404)
        return res.send({
          message: ERROR_MESSAGES.CONTACT_NOT_FOUND
        })
      }
      res.status(204).send({})
    } catch (e) {
      console.log("DELETE /contacts error", e)
      next(e)
    }
  }
);

router.get('/contacts/:contactId', async (req, res, next) => {
    try {
      const result = await getContactDetail(req.params.contactId)
      if (!result) {
        res.status(404)
        return res.send({
          message: ERROR_MESSAGES.CONTACT_NOT_FOUND
        })
      }
      res.send(result)
    } catch (e) {
      console.log("GET /contacts/:contactId error", e)
      next(e)
    }
  }
);

router.get('/contacts', async (req, res, next) => {
    try {
      const result = await getContracts(
        Number(req.query.begin) || 0
        , Number(req.query.total) || 20)
      res.send(result)
    } catch (e) {
      console.log("GET /contacts error", e)
      next(e)
    }
  }
);

module.exports = router