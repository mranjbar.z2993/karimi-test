FROM node:10-alpine
#Heroku runs docker apps as a non-root user
RUN adduser -D renjer

WORKDIR /usr/src/app
COPY src src
COPY package.json .
RUN npm i
USER renjer

CMD npm start


