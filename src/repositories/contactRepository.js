const ObjectId = require('mongoose').Types.ObjectId
const contactModel = require('../db/contactSchema').contractModel

function createNewContact(data) {
  return new contactModel(data).save()
}

function updateContractById(contactId, data) {
  return contactModel.findOneAndUpdate({_id: ObjectId(contactId)}, data, {
    new: true
  })
}

function findContractById(contactId, data) {
  return contactModel.findOne({_id: ObjectId(contactId)})
}
function deleteContactById(contactId, data) {
  return contactModel.deleteOne({_id: ObjectId(contactId)})
}

function listContacts(begin, total) {
  return contactModel.find({}).skip(begin).limit(total)
}

module.exports = {
  createNewContact,
  updateContractById,
  findContractById,
  deleteContactById,
  listContacts
}