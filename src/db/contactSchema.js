const mongoose = require('mongoose');

const contractSchema = new mongoose.Schema({
    firstName: {type: String, required:true},
    lastName: {type: String, required:true},
    email: {type: String, required:true},
    phone: {type: String, required:true},

    //java script data format
    date: {type: String, required:true},
    age: {type: Number, required: true},
    country: {type: String, enum: ["Australia", "Canada", "United Kingdom"]}
  });


const contractModel = mongoose.model('contract', contractSchema, 'contract')
module.exports = {contractModel};